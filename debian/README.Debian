sendxmpp for Debian
-------------------

Since 2.14 version, sendxmpp is using a new config file format. The file name
must be ~/.sendxmpprc with 400 permission. See below an example of the new
format:

  username: john
  jserver: jabber.example.com
  port: 5222
  password: my-very-nice-pass2

There are some tips for sendxmpp 2.14, when using cryptography (I believe these
tips will work for 2.13 version too). The following topics will require a
special attention.

1. When using sendxmpp, I get:

  "Could not connect to 'jabber.example.com' on port 5222: The server requires
   us to use TLS, but you did not specify that TLS was an option."

  To solve this problem, use -t option.

2. When sending a message, the following error is shown:

  "Invalid or unreadable path specified for ssl_ca_path. at
   /usr/share/perl5/XML/Stream.pm line 641."

  Use '-a /etc/ssl/certs/' or '--tls-ca-path /etc/ssl/certs' option.

3. I am getting several messages, similar to lines shown below:

  "Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1829.
   Use of uninitialized value $sid in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 2740.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 2742.
   Use of uninitialized value $sid in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 1668.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1669.
   Use of uninitialized value in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 1669.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1671.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1673.
   Use of uninitialized value in numeric eq (==) at /usr/share/perl5/XML/Stream.pm line 1673.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1675.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1678.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 2620.
   Use of uninitialized value $sid in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 2740.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 2742.
   Use of uninitialized value in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 1440.
   Use of uninitialized value in numeric eq (==) at /usr/share/perl5/XML/Stream.pm line 1443.
   Use of uninitialized value within %status in numeric eq (==) at /usr/share/perl5/XML/Stream.pm line 1506.
   Use of uninitialized value in subtraction (-) at /usr/share/perl5/XML/Stream.pm line 1507.
   Use of uninitialized value in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 1669.
   Use of uninitialized value in numeric eq (==) at /usr/share/perl5/XML/Stream.pm line 1673.
   Use of uninitialized value in hash element at /usr/share/perl5/Net/XMPP/Connection.pm line 433.
   Use of uninitialized value in hash element at /usr/share/perl5/Net/XMPP/Connection.pm line 440.
   Use of uninitialized value in hash element at /usr/share/perl5/Net/XMPP/Connection.pm line 433.
   Use of uninitialized value in string eq at /usr/bin/sendxmpp line 515.
   Error 'AuthSend': [?]
   Use of uninitialized value $sid in concatenation (.) or string at /usr/share/perl5/XML/Stream.pm line 1668.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1669.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1671.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1673.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1266.
   Use of uninitialized value in string eq at /usr/share/perl5/XML/Stream.pm line 1266.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1266.
   Use of uninitialized value in string eq at /usr/share/perl5/XML/Stream.pm line 1266.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1267.
   Use of uninitialized value in delete at /usr/share/perl5/XML/Stream.pm line 1267.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1268.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1270.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1270.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1270.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1270.
   Use of uninitialized value $sid in hash element at /usr/share/perl5/XML/Stream.pm line 1270.
   Use of uninitialized value $sid in delete at /usr/share/perl5/XML/Stream.pm line 1272."

  To solve:

  # apt-get install ca-certificates
  ---> Copy the root certificate from your CA to /usr/local/share/ca-certificates/.
       The file must use te suffix .crt, e.g. mycert.crt
  # update-ca-certificates
  ---> For more details: $ man update-ca-certificates

4. I am seeing an unknown behaviour.

  Add the '-v' option to your command line to try debug and find the problem.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 18 Jul 2017 10:38:30 -0300
